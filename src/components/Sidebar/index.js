import React, { Component } from 'react';
import PropTypes from 'prop-types';
import api from '../../services/api';

import { Container, Form, RepositoryItem } from './styles';

export default class Sidebar extends Component {
  static propTypes = {
    handleSelectRepository: PropTypes.func.isRequired,
  };

  state = {
    repositories: [],
    newRepository: '',
    activeRepositoryId: null,
    loading: false,
    error: false,
  };

  handleAddRepository = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });

    try {
      const { repositories, newRepository } = this.state;
      const { data: repository } = await api.get(`/repos/${newRepository}`);

      if (!repositories.find(repositoryItem => repositoryItem.id === repository.id)) {
        this.setState({
          repositories: [...repositories, repository],
        });
      }

      this.setState({
        newRepository: '',
        error: false,
      });
    } catch (err) {
      this.setState({ error: true });
    } finally {
      this.setState({ loading: false });
    }
  };

  handleInputChange = (e) => {
    this.setState({ newRepository: e.target.value });
  };

  handleClickRepository = (repository) => {
    const { handleSelectRepository } = this.props;

    this.setState({ activeRepositoryId: repository.id });

    handleSelectRepository(repository);
  };

  render() {
    const {
      repositories, activeRepositoryId, newRepository, error, loading,
    } = this.state;

    return (
      <Container>
        <Form hasError={error} onSubmit={this.handleAddRepository}>
          <input
            type="text"
            value={newRepository}
            onChange={this.handleInputChange}
            placeholder="Novo repositório"
          />
          <button type="submit">
            {loading ? (
              <i className="fa fa-spinner fa-spin" />
            ) : (
              <i className="fa fa-plus-circle" />
            )}
          </button>
        </Form>
        <div>
          {repositories.map(repository => (
            <RepositoryItem
              key={repository.id}
              type="button"
              active={repository.id === activeRepositoryId}
              onClick={() => this.handleClickRepository(repository)}
            >
              <div>
                <img src={repository.owner.avatar_url} alt={repository.full_name} />
                <div className="info">
                  <strong>{repository.name}</strong>
                  <span>{repository.owner.login}</span>
                </div>
              </div>
            </RepositoryItem>
          ))}
        </div>
      </Container>
    );
  }
}
