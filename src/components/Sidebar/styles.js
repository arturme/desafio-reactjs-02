import styled from 'styled-components';

export const Container = styled.aside`
  background: #fff;
  width: 320px;
  height: 100%;
  padding: 30px;
  box-shadow: 1px 0px 10px 0px #ddd;
  float: left;
  position: relative;
  z-index: 1;
`;

export const Form = styled.form`
  width: 100%;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
  margin-bottom: 20px;
  overflow: hidden;

  input {
    width: 210px;
    float: left;
    border: ${({ hasError }) => (hasError ? '2px solid #F00' : '0')};
    height: 40px;
    background: #eee;
    border-radius: 3px;
    padding: 12px 20px;
    font-size: 14px;
  }

  button {
    float: right;
    height: 40px;
    width: 40px;
    background: #59ea9a;
    border: 0;
    border-radius: 3px;
    font-size: 17px;
    color: #fff;
    transition: 0.3s all;
    cursor: pointer;

    &:hover {
      background: #39d880;
    }
  }
`;

export const RepositoryItem = styled.button`
  width: 100%;
  margin-bottom: 20px;
  cursor: pointer;
  position: relative;
  border: 0;
  text-align: left;
  opacity: ${({ active }) => (active ? 1 : 0.6)};

  img {
    width: 45px;
    height: 45px;
    float: left;
    margin-right: 10px;
  }

  .info {
    float: left;

    strong {
      display: block;
      margin: 5px 0 1px 0;
      font-size: 15px;
    }

    span {
      display: block;
      color: #666;
    }
  }

  > div {
    overflow: hidden;
  }

  > div:after {
    content: '\f105';
    font-family: FontAwesome;
    position: absolute;
    right: 0;
    top: 14px;
    color: #666;
    transition: 0.3s all;
    font-size: 15px;
  }

  &:hover {
    opacity: 1;
  }
`;
