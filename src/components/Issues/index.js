import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Container, Loading, IssuesList } from './styles';
import api from '../../services/api';

export default class Issues extends Component {
  availableFilters = [
    { value: 'all', label: 'Todas' },
    { value: 'open', label: 'Abertas' },
    { value: 'closed', label: 'Fechadas' },
  ];

  static propTypes = {
    repository: PropTypes.shape({
      name: PropTypes.string,
      full_name: PropTypes.string,
      owner: PropTypes.shape({
        avatar_url: PropTypes.string,
        login: PropTypes.string,
      }),
    }).isRequired,
  };

  state = {
    activeFilter: 'all',
    loading: false,
    issues: [],
  };

  // Carrega issues quando usuário seleciona repositório
  componentDidMount() {
    this.loadIssues();
  }

  // Atualiza issues quando usuário selecionar outro repositório
  componentDidUpdate(props) {
    const { repository } = this.props;

    if (props.repository.id !== repository.id) {
      this.loadIssues();
    }
  }

  loadIssues = async () => {
    const { repository } = this.props;
    const { activeFilter } = this.state;

    this.setState({ loading: true });

    try {
      const response = await api.get(`repos/${repository.full_name}/issues`, {
        params: { state: activeFilter },
      });

      this.setState({ issues: response.data });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({ loading: false });
    }
  };

  handleFilterChange = (e) => {
    this.setState({ activeFilter: e.target.value }, this.loadIssues);
  };

  render() {
    const { issues, loading } = this.state;
    const { repository } = this.props;

    return (
      <Container>
        <header>
          <div>
            <img src={repository.owner.avatar_url} alt={repository.full_name} />
            <div className="info">
              <strong>{repository.name}</strong>
              <span>{repository.owner.login}</span>
            </div>
          </div>
          <select onChange={this.handleFilterChange}>
            {this.availableFilters.map(filter => (
              <option key={filter.value} value={filter.value}>
                {filter.label}
              </option>
            ))}
          </select>
        </header>
        <main>
          {loading ? (
            <Loading>
              <i className="fa fa-spinner fa-pulse" />
            </Loading>
          ) : (
            <IssuesList>
              {issues.map(issue => (
                <li key={issue.id}>
                  <img src={issue.user.avatar_url} alt={issue.user.login} />
                  <div>
                    <strong>{issue.title}</strong>
                    <span>{issue.user.login}</span>
                    <a href={issue.html_url} target="_blank" rel="noopener noreferrer">
                      Abrir issue
                    </a>
                  </div>
                </li>
              ))}
            </IssuesList>
          )}
        </main>
      </Container>
    );
  }
}
