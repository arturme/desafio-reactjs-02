import styled from 'styled-components';

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  height: 100%;

  header {
    padding: 30px;
    box-shadow: 8px -1px 7px 4px #ddd;
    background: #fff;
    z-index: 0;
    position: relative;

    > div {
      overflow: hidden;
      float: left;

      img {
        width: 45px;
        height: 45px;
        float: left;
        margin-right: 10px;
      }

      .info {
        float: left;

        small {
          display: block;
          color: #666;
        }

        strong {
          display: block;
          margin: 5px 0 1px 0;
        }
      }
    }

    select {
      height: 42px;
      border: 1px solid #ddd;
      padding: 0 15px;
      width: 150px;
      font-size: 14px;
      float: right;
    }
  }

  main {
    height: 100%;
    background: #f1f1f1;
    padding: 30px;
  }
`;

export const Loading = styled.div``;

export const IssuesList = styled.ul`
  list-style: none;
  padding: 0 0 30px;
  margin: 0;
  display: flex;
  flex-wrap: wrap;

  li {
    width: 30%;
    padding: 20px;
    margin: 10px;
    display: flex;
    flex: 1;
    flex-direction: row;
    background: #fff;
    border-radius: 3px;
    box-shadow: 1px 1px 5px 1px #d4d4d4;

    img {
      width: 64px;
      height: 64px;
      margin-right: 10px;
      border-radius: 32px;
      flex-shrink: 0;
    }

    div {
      display: flex;
      flex-direction: column;
      overflow: hidden;
      width: 100%;

      strong {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      span {
        margin: 3px 0 8px 0;
        font-size: 12px;
        color: #666;
      }

      a {
        background: #b286d1;
        color: #fff;
        text-decoration: none;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 13px;
        padding: 0 20px 0 35px;
        height: 29px;
        line-height: 29px;
        display: inline-block;
        border-radius: 3px;
        position: relative;
        transition: 0.3s all;
        text-align: center;

        &:before {
          position: absolute;
          left: 14px;
          font-family: FontAwesome;
          content: '\f08e';
        }

        &:hover {
          background: #9d67c3;
        }
      }
    }
  }
`;
