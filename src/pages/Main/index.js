import React, { Component, Fragment } from 'react';

import Sidebar from '../../components/Sidebar';
import Issues from '../../components/Issues';

export default class Main extends Component {
  state = {
    activeRepository: null,
  };

  handleSelectRepository = (repository) => {
    this.setState({ activeRepository: repository });
  };

  render() {
    const { activeRepository } = this.state;

    return (
      <Fragment>
        <Sidebar handleSelectRepository={this.handleSelectRepository} />
        {activeRepository && <Issues repository={activeRepository} />}
      </Fragment>
    );
  }
}
